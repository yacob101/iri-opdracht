#include <stdio.h>
#include <stdbool.h>

struct binarySensor
{
    char name[40];
    bool value;
    time_t epochTime;
	char alarm[40];
};

struct analogSensor
{
    char name[40];
    int value;
    char unit[15];
    int epochTime;
    int lowerBound;
    int higherBound;
	bool lowerBoundAlarm;
    bool higherBoundAlarm;
};


 static int changedSensorList[3000];
 static int changedSensorLastIndex = 0;

void generateSensors(char *sensorJson[], char *changedValueString[]);
char** str_split(char* a_str, const char a_delim);