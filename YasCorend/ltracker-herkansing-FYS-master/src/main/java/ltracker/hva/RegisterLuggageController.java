/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ltracker.hva;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ltracker.hva.classes.FoundLuggage;
import ltracker.hva.database.MyJDBC;

/**
 * FXML Controller class
 *
 * @author Admin
 */
public class RegisterLuggageController implements Initializable {
    
FoundLuggage luggage = new FoundLuggage();

    @FXML
    private Button saveButton;
    @FXML
    private TextField lostAndFoundID;
    @FXML
    private TextField flightNumber;
    @FXML
    private TextField timeFound;
    @FXML
    private ComboBox<String> airportArrival;
    @FXML
    private DatePicker dateFound;
    @FXML
    private TextField airportDeparture;
    @FXML
    private TextField passengerName;
    @FXML
    private TextField passengerCity;
    @FXML
    private TextField labelNumber;
    @FXML
    private TextField typeLuggage;
    @FXML
    private TextField brandLuggage;
    @FXML
    private TextField color;
    @FXML
    private TextField specialCharacteristics;

   
    
    @FXML
    private void saveButtonClicked(ActionEvent event) {
        MyJDBC myjdbc = new MyJDBC("corendon");

       

        // todo, copy schermvelden naar luggage
        luggage.setLostAndFoundID(lostAndFoundID.getText());
        luggage.setFlightNumber(flightNumber.getText());
        luggage.setTimeFound(timeFound.getText());
        luggage.setAirportDeparture(airportDeparture.getText());
        luggage.setDateFound(dateFound.getValue());
        luggage.setAirportArrival(airportArrival.getValue());
        luggage.setPassengerName(passengerName.getText());
        luggage.setPassengerCity(passengerCity.getText());
        luggage.setLabelNumber(labelNumber.getText());
        luggage.setTypeLuggage(typeLuggage.getText());
        luggage.setBrandLuggage(brandLuggage.getText());
        luggage.setColor(color.getText());
        luggage.setSpecialCharacteristics(specialCharacteristics.getText());

        luggage.insertFoundLuggage(myjdbc);
    }
    

    @FXML
    private void searchButtonRegisterPushed(ActionEvent event) throws IOException {
        Parent registerLuggageParent = FXMLLoader.load(getClass().getResource("mainScene.fxml"));
        Scene registLuggageScene = new Scene(registerLuggageParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(registLuggageScene);
        window.show();

    }

    @FXML
    private void statisticsButtonRegisterPushed(ActionEvent event) throws IOException {
        Parent registerLuggageParent = FXMLLoader.load(getClass().getResource("statistics.fxml"));
        Scene registLuggageScene = new Scene(registerLuggageParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(registLuggageScene);
        window.show();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    try {
        luggage.getAirportList(airportArrival);
        // TODO
    } catch (IOException | SQLException ex) {
        Logger.getLogger(RegisterLuggageController.class.getName()).log(Level.SEVERE, null, ex);
    }
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public void setSaveButton(Button saveButton) {
        this.saveButton = saveButton;
    }

    public TextField getLostAndFoundID() {
        return lostAndFoundID;
    }

    public void setLostAndFoundID(TextField lostAndFoundID) {
        this.lostAndFoundID = lostAndFoundID;
    }

    public TextField getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(TextField flightNumber) {
        this.flightNumber = flightNumber;
    }

    public TextField getTimeFound() {
        return timeFound;
    }

    public void setTimeFound(TextField timeFound) {
        this.timeFound = timeFound;
    }

    public ComboBox getAirportArrival() {
        return airportArrival;
    }

    public void setAirportArrival(ComboBox airportArrival) {
        this.airportArrival = airportArrival;
    }

    public DatePicker getDateFound() {
        return dateFound;
    }

    public void setDateFound(DatePicker dateFound) {
        this.dateFound = dateFound;
    }

    public TextField getAirportDeparture() {
        return airportDeparture;
    }

    public void setAirportDeparture(TextField airportDestination) {
        this.airportDeparture = airportDestination;
    }
    

    public TextField getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(TextField passengerName) {
        this.passengerName = passengerName;
    }

    public TextField getPassengerCity() {
        return passengerCity;
    }

    public void setPassengerCity(TextField passengerCity) {
        this.passengerCity = passengerCity;
    }

    public TextField getLabelNumber() {
        return labelNumber;
    }

    public void setLabelNumber(TextField labelNumber) {
        this.labelNumber = labelNumber;
    }

    public TextField getTypeLuggage() {
        return typeLuggage;
    }

    public void setTypeLuggage(TextField typeLuggage) {
        this.typeLuggage = typeLuggage;
    }

    public TextField getBrandLuggage() {
        return brandLuggage;
    }

    public void setBrandLuggage(TextField brandLuggage) {
        this.brandLuggage = brandLuggage;
    }

    public TextField getColor() {
        return color;
    }

    public void setColor(TextField color) {
        this.color = color;
    }

    public TextField getSpecialCharacteristics() {
        return specialCharacteristics;
    }

    public void setSpecialCharacteristics(TextField specialCharacteristics) {
        this.specialCharacteristics = specialCharacteristics;
    }

 

}
