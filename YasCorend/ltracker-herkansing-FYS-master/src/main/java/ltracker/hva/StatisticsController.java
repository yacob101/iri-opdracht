/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ltracker.hva;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Admin
 */
public class StatisticsController implements Initializable {

    @FXML
    Button registerScreenStatisticsButton;

    /**
     * Initializes the controller class.
     */

    
    
    @FXML
    private void registerButtonStatisticsPushed(ActionEvent event) throws IOException {
        Parent registerLuggageParent = FXMLLoader.load(getClass().getResource("registerLuggage.fxml"));
        Scene registLuggageScene = new Scene(registerLuggageParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(registLuggageScene);
        window.show();

    }
    
     @FXML
    private void searchButtonStatisticsPushed(ActionEvent event) throws IOException {
        Parent registerLuggageParent = FXMLLoader.load(getClass().getResource("mainScene.fxml"));
        Scene registLuggageScene = new Scene(registerLuggageParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(registLuggageScene);
        window.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
