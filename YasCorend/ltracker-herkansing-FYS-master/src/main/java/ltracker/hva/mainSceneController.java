package ltracker.hva;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import ltracker.hva.classes.FoundLuggage;
import ltracker.hva.database.MyJDBC;
import ltracker.hva.classes.FoundLuggage;
import ltracker.hva.classes.Product;
import ltracker.hva.classes.airportList;

public class mainSceneController implements Initializable {
//Buttons to switch scenes.
//    Product lug = new Product();

    @FXML
    Button registerSceneButton;
    @FXML
    Button statisticsSceneButton;

    //Tableview
    @FXML
    private TableView<Product> table;
    @FXML
    private TableColumn<Product, String> date;
    @FXML
    private TableColumn<Product, String> time;
    @FXML
    private TableColumn<Product, String> name;
    @FXML
    private TableColumn<Product, String> city;
    @FXML
    private TableColumn<Product, String> airportDeparture;
    @FXML
    private TableColumn<Product, String> flightNumber;
    @FXML
    private TableColumn<Product, String> type;
    @FXML
    private TableColumn<Product, String> brand;
    @FXML
    private TableColumn<Product, String> color;

//    .addAll(name, time, passengerCity, airportDeparture, flightNumber, type, brand, color);

    //When register button is pushed switch scenes
    @FXML
    private void registerButtonPushed(ActionEvent event) throws IOException, SQLException {
        FoundLuggage luggage = new FoundLuggage();

        Parent registerLuggageParent = FXMLLoader.load(getClass().getResource("/ltracker/hva/registerLuggage.fxml"));
        Scene registerLuggageScene = new Scene(registerLuggageParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(registerLuggageScene);
        window.show();

    }

    @FXML
    private void statisticsButtonPushed(ActionEvent event) throws IOException {

        Parent statisticsParent = FXMLLoader.load(getClass().getResource("/ltracker/hva/statistics.fxml"));
        Scene statisticsScene = new Scene(statisticsParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(statisticsScene);
        window.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {

        date.setCellValueFactory(new PropertyValueFactory<Product, String>("date"));
        time.setCellValueFactory(new PropertyValueFactory<Product, String>("time"));
        name.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));   
        
        city.setCellValueFactory(new PropertyValueFactory<Product, String> ("passengerCity"));
        
        airportDeparture.setCellValueFactory(new PropertyValueFactory<Product, String>("airportDeparture"));
        flightNumber.setCellValueFactory(new PropertyValueFactory<Product, String>("flightNumber"));
        type.setCellValueFactory(new PropertyValueFactory<Product, String>("type"));
        brand.setCellValueFactory(new PropertyValueFactory<Product, String>("brand"));
        color.setCellValueFactory(new PropertyValueFactory<Product, String>("color"));
        
        try {
            table.setItems(getProduct());
        } catch (SQLException ex) {
            Logger.getLogger(mainSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }

    public ObservableList<Product> getProduct() throws SQLException {
        MyJDBC myJDBC = new MyJDBC("corendon");
        ObservableList<Product> products = FXCollections.observableArrayList();
        ResultSet rs = myJDBC.executeResultSetQuery(
                "SELECT dateFound, timeFound, passengerName, passengerCity, airportDeparture, flightNumber, typeLuggage, brandLuggage, color FROM foundLuggage");
        while (rs.next()) {
            // add each item from the ResultSet to the ComboBox as a string
            products.add(new Product(rs.getDate("dateFound").toString(), rs.getString("timeFound"), rs.getString("passengerName"), rs.getString("passengerCity"), rs.getString("airportDeparture"), rs.getString("flightNumber"), rs.getString("typeLuggage"), rs.getString("brandLuggage"), rs.getString("color")));

        }
        return products;
    }
}
